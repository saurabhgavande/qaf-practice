package com.qmetry.newtours.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class Login extends WebDriverBaseTestPage<WebDriverTestPage>{

	@FindBy(locator="login.username.text")
	private QAFWebElement username;
	
	@FindBy(locator="login.password.text")
	private QAFWebElement password;
	
	@FindBy(locator="login.signin.button")
	private QAFWebElement signin;
	
	public QAFWebElement getUsername() {
		return username;
	}

	public QAFWebElement getPassword() {
		return password;
	}

	public QAFWebElement getSignin() {
		return signin;
	}
	
	public void dologin(String username, String password) {
		getUsername().sendKeys(username);
		getPassword().sendKeys(password);
		getSignin().click();
		
		
	}
	
	
	

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		driver.get("/");
	}

}
